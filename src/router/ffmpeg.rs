use super::model::{FrameMessage, Status};
use futures::TryFutureExt;
use std::{
    path::{Path, PathBuf},
    process::Stdio,
    sync::{atomic, atomic::AtomicBool, Arc},
};
use tokio::{
    fs::{self, File},
    io,
    process::{Child, Command},
    sync::{
        mpsc::{self, Receiver, Sender},
        Notify,
    },
};
use tracing::Instrument;

type TokioSendResult<T> = Result<(), tokio::sync::mpsc::error::SendError<T>>;

pub struct EncoderHandle {
    task: tokio::task::JoinHandle<io::Result<bool>>,
    done: Arc<AtomicBool>,

    to_encoder:    Sender<FrameMessage>,
    notify_cancel: Arc<Notify>,
}

impl EncoderHandle {
    pub async fn new_frame(&self, from: usize, to: usize) -> TokioSendResult<FrameMessage> {
        self.to_encoder
            .send(FrameMessage::Frame { idx: from, to })
            .await
    }

    pub async fn stop(&self) -> TokioSendResult<FrameMessage> {
        self.to_encoder.send(FrameMessage::Stop).await
    }

    pub fn cancel(&self) {
        // TODO: use notify instead of normal message queue
        self.notify_cancel.notify_one()
    }

    pub async fn is_done(&self) -> bool { self.done.load(atomic::Ordering::Relaxed) }

    pub async fn done(self) -> io::Result<bool> {
        self.task
            .await
            .map_err(|e| io::Error::new(io::ErrorKind::Other, e))
            .flatten()
    }
}

/// Takes a string that points to a file. As the file might not exists it is
/// first popped off the path before canonicalizing it.
async fn canonicalize_directory_with_file(p: &str) -> io::Result<PathBuf> {
    let mut dirname = PathBuf::from(p);
    let file_name = dirname.file_name().unwrap().to_string_lossy().to_string();
    dirname.pop();
    dirname = fs::canonicalize(&dirname).await?;
    Ok(dirname.join(file_name))
}

pub struct Encoder {
    ffmpeg:      Child,
    done:        Arc<AtomicBool>,
    file_target: PathBuf,
    base_path:   PathBuf,
    rx:          Receiver<FrameMessage>,
    cancel:      Arc<Notify>,
    status_info: Status,
}

impl Encoder {
    #[instrument(skip(self))]
    async fn worker(mut self) -> io::Result<bool> {
        {
            let mut lock = self.status_info.data().lock();
            lock.current_file = Some(format!("{}", self.file_target.display()));
            lock.status = super::model::StatusState::Recording;
        };

        let mut stdin = self.ffmpeg.stdin.take().unwrap();
        let mut is_cancel = false;

        let exit_status = 'enc_loop: loop {
            tokio::select! {
                biased;
                _ = self.cancel.notified() => {
                    is_cancel = true;
                    warn!("recieved cancel message, closing streams and removing artifacts");
                    self.status_info.data().lock().status = super::model::StatusState::Canceled;
                    break 'enc_loop Ok(true);
                },
                exit_status = self.ffmpeg.wait() => {
                    is_cancel = true;
                    self.status_info.data().lock().status = super::model::StatusState::Stopped;
                    break 'enc_loop exit_status.map(|exit|
                        { warn!(exit.status=%exit, "ffmpeg child exited, closing handle"); exit.success() });
                },

                v = self.rx.recv() => {
                    trace!(message=?v);
                    match v {
                        None => {
                            warn!("channel returned none, assuming it got closed from the other end");
                            break 'enc_loop Ok(true);
                        },
                        Some(FrameMessage::Frame { idx, to }) => {
                            debug!(f.from=%idx, f.to=%to, "recieved frame notification");

                            let mut frames_found = 0;
                            let mut errors: Vec<super::model::ErrorMessage> = Vec::with_capacity(((to + 1 - idx) / 2).max(1));
                            for i in idx..=to {
                                let current_frame = self.base_path.join(format!("frame{}.png", i));

                                // try to open frame, skipping it if it does not exist
                                let mut framef = match File::open(&current_frame).await {
                                    Ok(file) => file,
                                    Err(why) => match why.kind() {
                                        io::ErrorKind::NotFound => {
                                            error!(frame=%i, ?why, "could not find frame {}, but it should be ready", i);
                                            errors.push(statuserror!(&why, format_args!("could not find frame {}, but it should be ready", i)));
                                            continue;
                                        },
                                        _ => break 'enc_loop Err(why)
                                    }
                                };

                                // TODO: handle frame
                                if let Err(why) = tokio::io::copy(&mut framef, &mut stdin).await {
                                    error!(?why, "failed to io copy");
                                    errors.push(statuserror!(&why, "failed to copy frame to ffmpeg"));
                                    break 'enc_loop Err(why);
                                }

                                drop(framef);
                                frames_found += 1;

                                if let Err(why) = fs::remove_file(&current_frame).await {
                                    error!(?why, "failed to remove origin frame");
                                    errors.push(statuserror!(&why, "failed to remove frame artifact"));
                                    break 'enc_loop Err(why);
                                }
                            }

                            // if we have changed data, extend state
                            if frames_found > 0 || !errors.is_empty() {
                                let mut lock = self.status_info.data().lock();
                                lock.frames_recorded += frames_found;
                                // this should not lock us even when using an async function
                                lock.errors.extend_from_slice(&errors);
                            }
                        },
                        Some(FrameMessage::Stop) => {
                            info!("recieved stop message, closing streams");
                            self.status_info.data().lock().status = super::model::StatusState::Stopped;
                            break 'enc_loop Ok(true);
                        }
                    }
                }
            }
        };

        info!("encoding done, exiting ffmpeg");

        drop(stdin);
        let final_status = match exit_status {
            Ok(val) => self.ffmpeg.wait().await.map(|exit| exit.success() && val),
            Err(why) => Err(why),
        };

        debug!(?final_status);

        if is_cancel && Path::new(&self.file_target).exists() {
            if let Err(why) = fs::remove_file(self.file_target).await {
                error!(?why, "failed to remove unfinished output file");
            }
        }

        self.done.store(true, atomic::Ordering::Relaxed);

        final_status
    }

    #[instrument(skip(args, file_target, frame_source, status))]
    pub async fn start(
        ffmpeg: &str,
        args: &[&str],
        file_target: &str,
        frame_source: &str,
        status: Status,
    ) -> io::Result<EncoderHandle> {
        debug!("ffmpeg start request recieved");
        let file_target = canonicalize_directory_with_file(file_target).await?;
        let ft = format!("{}", &file_target.display());

        trace!(real_target=%ft);

        if file_target.exists() {
            info!("target file exists, renaming to $FILE.old");
            let mut dirname = file_target.clone();
            let file_name = dirname.file_name().unwrap().to_string_lossy().to_string();
            dirname.pop();

            if let Err(why) =
                fs::rename(&file_target, dirname.join(format!("{}.old", file_name))).await
            {
                error!(
                    ?why,
                    "could not rename existing file, and ffmpeg refuses to overwrite"
                );
                return Err(why);
            };
        }

        debug!("initializing ffmpeg");
        let ffmpeg = Command::new(ffmpeg)
            .args(args)
            .stderr(Stdio::null())
            .stdout(Stdio::null())
            .stdin(Stdio::piped())
            .spawn()?;
        let base_path = PathBuf::from(frame_source);

        let result_bool = Arc::new(AtomicBool::new(false));
        let done = Arc::clone(&result_bool);

        debug!("spawning handler");

        let (msg_tx, msg_rx) = mpsc::channel(1024);

        let notify = Arc::new(Notify::new());

        let encoder = Encoder {
            ffmpeg,
            done,
            file_target,
            base_path,
            rx: msg_rx,
            cancel: Arc::clone(&notify),
            status_info: status,
        };

        let handle = tokio::spawn(
            encoder
                .worker()
                .instrument(error_span!("ffmpeg_encode", target=?ft)),
        );

        Ok(EncoderHandle {
            task:          handle,
            done:          result_bool,
            to_encoder:    msg_tx,
            notify_cancel: notify,
        })
    }
}

// attempt to discover ffmpeg
// search order is this, prefering a local ffmpeg over a global one:
// - ./ffmpeg
// - ./bin/ffmeg
// - ffmpeg
#[instrument]
pub async fn discover_ffmpeg() -> io::Result<String> {
    debug!("discovering ffmpeg");
    let p = PathBuf::from(".");

    // ./ffmpeg
    trace!("trying ./ffmpeg");
    let mut pp = p.join("ffmpeg");
    if pp.exists() {
        debug!("ffmpeg is at {}", pp.display());
        return Ok(pp.to_string_lossy().to_string());
    }

    // ./bin/ffmpeg
    trace!("trying ./bin/ffmpeg");
    pp = p.join("bin").join("ffmpeg");
    if pp.exists() {
        debug!("ffmpeg is at {}", pp.display());
        return Ok(pp.to_string_lossy().to_string());
    }

    trace!("no local ffmpeg, trying process env");
    let _cc = Command::new("ffmpeg")
        .arg("-version")
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .spawn()?
        .wait()
        .inspect_err(|e| warn!(why=?e, "could not find ffmpeg"))
        .await?;

    debug!("ffmpeg is in env at ffmpeg");
    Ok(String::from("ffmpeg"))
}
