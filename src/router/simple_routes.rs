use hyper::{header, header::HeaderValue, Body, Request, Response, StatusCode};
use routerify::ext::RequestExt;

use super::sfile::SFile;

use super::Error;

pub async fn static_files(req: Request<Body>) -> Result<Response<Body>, Error> {
    static HEALTH_CSS: SFile = SFile::new(include_str!("web/health.css"));
    static HEALTH_JS: SFile = SFile::new(include_str!("web/health.js"));
    static BULMA_CSS: SFile = SFile::new(include_str!("web/bulma.min.css"));

    let file: &'static SFile = match req.param("file").map(|i| i.as_str()) {
        Some("bulma.min.css") => &BULMA_CSS,
        Some("health.css") => &HEALTH_CSS,
        Some("health.js") => &HEALTH_JS,
        _ => {
            return Response::builder()
                .status(StatusCode::NOT_FOUND)
                .body(Body::empty())
                .map_err(From::from)
        },
    };

    match req.headers().get(header::IF_NONE_MATCH) {
        Some(tag) if tag == file.etag() => Response::builder()
            .status(StatusCode::NOT_MODIFIED)
            .body(Body::empty())
            .map_err(From::from),
        _ => Response::builder()
            .status(StatusCode::OK)
            .header(header::ETAG, HeaderValue::from_str(file.etag())?)
            .body(Body::from(file.content()))
            .map_err(From::from),
    }
}

pub async fn alive(_: Request<Body>) -> Result<Response<Body>, Error> {
    Ok(Response::builder()
        .status(StatusCode::OK)
        .body(Body::from("yes"))
        .unwrap())
}
