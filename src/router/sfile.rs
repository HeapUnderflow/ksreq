use once_cell::sync::OnceCell;
use sha2::Digest;

pub struct SFile {
    hash: OnceCell<String>,
    file: &'static str,
}

impl SFile {
    pub const fn new(content: &'static str) -> Self {
        Self {
            hash: OnceCell::new(),
            file: content,
        }
    }

    pub fn content(&self) -> &'static str { self.file }

    pub fn etag(&self) -> &str {
        self.hash
            .get_or_init(|| format!("{:x}", sha2::Sha256::digest(self.file.as_bytes())))
    }
}
