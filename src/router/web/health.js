(function ($window, $document) {
    if ($window.location.hash !== "#noreload") {
        setTimeout(function () {
            $window.location.reload();
        }, 30000);
    }
})(window, document);