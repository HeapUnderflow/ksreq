use std::fmt;
use crate::router::model::{ErrorMessage, StatusState};

use super::{
    model::Status,
    Error,
};
use futures::{stream::StreamExt, TryStreamExt};
use hyper::{
    header::{self, HeaderValue},
    Body,
    Request,
    Response,
    StatusCode,
};
use routerify::ext::RequestExt;

fn render_health(status: &Status) -> maud::PreEscaped<String> {
    fn render_internal(
        state: StatusState,
        file: &str,
        frames_now: usize,
        errors: &[ErrorMessage]
    ) -> maud::PreEscaped<String> {
        maud::html! {
            section#sect_status {
                table.table.is-hoverable {
                    thead { tr { th { "Status" }; th { "=>" }; th {
                        @match state {
                            StatusState::Waiting   => { span.tag.is-warning { "Waiting"   } },
                            StatusState::Recording => { span.tag.is-success { "Recording" } },
                            StatusState::Stopped   => { span.tag            { "Stopped"   } },
                            StatusState::Canceled  => { span.tag.is-danger  { "Canceled"  } }
                        }
                    }; }; }
                    tr { td { "Target File" }; td { "=>" }; td {
                            @if file != "-" {
                                a href={ "file://" (file.replace("\\", "/")) } {
                                    (file)
                                }
                            } @else {
                                "-"
                            }
                        };
                    };
                    tr { td { "Processed Frames" }; td { "=>" }; td { span.tag { (frames_now) } }; };
                };
            }

            section.is_large#sect_errors {
                h1.title { "Errors" }
                @if errors.is_empty() {
                    div.notification.is-success {
                        "No Errors"
                    }
                } @else {
                    table.table.is-hoverable {
                        thead {
                            tr { th { "Time" }; th { "Location" }; th { "Error" } };
                        };
                        @for error in errors {
                            tr { td { (error.time()) }; td { code { (error.location()) } }; td { (error.message()) } };
                        }
                    }
                }
            }
        }
    }

    let data = status.data().lock().clone();
    render_internal(
        data.status,
        data.current_file.as_deref().unwrap_or("-"),
        data.frames_recorded,
        &data.errors
    )
}

pub async fn health(req: Request<Body>) -> Result<Response<Body>, Error> {
    const HS_STATUS_HEADER: &str = concat!("Health status of ", env!("CARGO_PKG_NAME"));

    let status: &Status = match req.data() {
        Some(data) => data,
        None => {
            return Err(Error::NoStateData);
        },
    };

    let htm = maud::html! {
        (maud::DOCTYPE);
        html lang="en" {
            head {
                meta charset="UTF-8";
                meta http-equiv="X-UA-Compatible" content="IE=edge";
                meta name="viewport" content="width=device-width, initial-scale=1.0";


                link rel="stylesheet" href="static/bulma.min.css";
                link rel="stylesheet" href="static/health.css";

                title { (HS_STATUS_HEADER) }
            };
            body {
                div.container {
                    h1.title { (HS_STATUS_HEADER) }
                    h2.subtitle { "Server " (crate::SERVER_VERSION)}
                    // render health & errors
                    (render_health(status));
                }

                script type="text/javascript" src="static/health.js" { };
            };
        };
    };

    let html = htm.into_string();

    let resp = Response::builder()
        .status(StatusCode::OK)
        .header(header::CONTENT_TYPE, HeaderValue::from_static("text/html"))
        .body(Body::from(html))?;
    Ok(resp)
}

#[derive(Debug, serde::Deserialize)]
struct StartData {
    file_source: String,
    output_file: String,
    ffmpeg:      Option<String>,

    ffmpeg_arg_in_resolution:  String,
    ffmpeg_arg_out_resolution: String,
    ffmpeg_arg_scaling:        Option<StartScalingArg>,
    ffmpeg_arg_framerate:      Option<String>,
    ffmpeg_arg_x264_preset:    Option<String>,
    ffmpeg_arg_pixfmt:         Option<String>,
    ffmpeg_arg_override:       Option<Vec<String>>,
}

#[derive(Debug, serde::Deserialize)]
#[serde(rename_all = "snake_case")]
enum StartScalingArg {
    Bicubic,
    Bilinear,
}

impl fmt::Display for StartScalingArg {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(match self {
            Self::Bicubic => "bicubic",
            Self::Bilinear => "bilinear",
        })
    }
}

const MAX_DATA_SIZE: usize = 1024 * 32; // 32kb

// ffmpeg arg constants
const FFMPEG_ARG_SCALING: StartScalingArg = StartScalingArg::Bicubic;
const FFMPEG_ARG_FRAMERATE: &str = "60";
const FFMPEG_ARG_X264_PRESET: &str = "medium";
const FFMPEG_ARG_PIXFMT: &str = "yuv420p";

macro_rules! check_access_cond {
	(@@internal, $req:ident, $ng:expr, $msg:expr) => {{
		if $req
            .data::<crate::router::model::Status>()
            .unwrap()
            .data()
            .lock()
            .status != $ng
        {
            return crate::router::make_error($msg).map_err(crate::router::Error::Hyper);
        }
	}};
	(@running $req:ident) => { check_access_cond!(@@internal, $req, crate::router::model::StatusState::Recording, "A encoding needs to be running to use this endpoint"); };
	(@waiting $req:ident) => { check_access_cond!(@@internal, $req, crate::router::model::StatusState::Waiting, "A encoding is already running."); };
}

pub async fn start(mut req: Request<Body>) -> Result<Response<Body>, Error> {
    check_access_cond!(@waiting req);

    let size_hint = req
        .headers()
        .get(header::CONTENT_LENGTH)
        .and_then(|val| val.to_str().ok()?.parse().ok())
        .unwrap_or(1024)
        .max(MAX_DATA_SIZE);

    let data = req
        .body_mut()
        .take(MAX_DATA_SIZE)
        .try_fold(
            Vec::with_capacity(size_hint),
            |mut state, data| async move {
                state.extend_from_slice(&*data);
                Ok(state)
            },
        )
        .await;

    let data = match data {
        Ok(data) => data,
        Err(why) => {
            return super::make_error(why).map_err(From::from);
        },
    };

    let config_data: StartData = match serde_json::from_slice(&data) {
        Ok(data) => data,
        Err(why) => return super::make_error(why).map_err(From::from),
    };

    trace!(?config_data, "decoded config payload");

    // declaring the string outsite of the scope, but not assigning it yet.
    // this is needed as the format would otherwise not live long enough to be
    // passed to the ffmpeg struct
    let vf_arg_cnt: String;
    let args = match &config_data.ffmpeg_arg_override {
        Some(ov) => ov.iter().map(|e| e.as_str()).collect::<Vec<_>>(),
        None => {
            vf_arg_cnt = format!(
                "scale={}:flags={}",
                config_data.ffmpeg_arg_out_resolution,
                config_data.ffmpeg_arg_scaling.unwrap_or(FFMPEG_ARG_SCALING)
            );
            vec![
                "-framerate",
                config_data
                    .ffmpeg_arg_framerate
                    .as_deref()
                    .unwrap_or(FFMPEG_ARG_FRAMERATE),
                "-s",
                &config_data.ffmpeg_arg_in_resolution,
                "-an",
                "-f",
                "image2pipe",
                "-i",
                "-",
                "-vcodec",
                "libx264",
                "-pix_fmt",
                config_data
                    .ffmpeg_arg_pixfmt
                    .as_deref()
                    .unwrap_or(FFMPEG_ARG_PIXFMT),
                "-preset",
                config_data
                    .ffmpeg_arg_x264_preset
                    .as_deref()
                    .unwrap_or(FFMPEG_ARG_X264_PRESET),
                "-vf",
                &vf_arg_cnt,
                &config_data.output_file,
            ]
        },
    };

    let ffmpeg_binary = match config_data.ffmpeg {
        Some(v) => v,
        None => match super::ffmpeg::discover_ffmpeg().await {
            Ok(v) => v,
            Err(why) => return super::make_error(why).map_err(From::from),
        },
    };

    debug!(ffmpeg.bin=%ffmpeg_binary, ffmpeg.args=?args, "starting ffmpeg");

    let status: Status = req.data().cloned().unwrap();

    let ffmpeg = super::ffmpeg::Encoder::start(
        &ffmpeg_binary,
        &args,
        &config_data.output_file,
        &config_data.file_source,
        status,
    )
    .await;
    match ffmpeg {
        Ok(handle) => *req.data::<super::model::State>().unwrap().stop.lock().await = Some(handle),
        Err(why) => return super::make_error(why).map_err(From::from),
    }

    Ok(Response::builder()
        .status(StatusCode::OK)
        .body(Body::empty())
        .unwrap())
}

pub async fn frame(req: Request<Body>) -> Result<Response<Body>, Error> {
    check_access_cond!(@running req);

    let param = match req.param("fspec") {
        Some(p) => p,
        None => return super::make_error("missing frame-spec as url argument").map_err(From::from),
    };

    if param.is_empty() {
        return super::make_error("cannot parse empty frame-spec").map_err(From::from);
    }

    let mut param_parts = param.split('-');
    let from: usize = match param_parts.next().map(|v| v.parse()) {
        Some(Ok(num)) => num,
        Some(Err(why)) => return super::make_error(why).map_err(From::from),
        None => unreachable!("param.is_empty should have caught this"),
    };

    let to: usize = match param_parts.next().map(|v| v.parse()) {
        Some(Ok(num)) => num,
        Some(Err(why)) => return super::make_error(why).map_err(From::from),
        // setting to = from when we have only 1 frame ready
        None => from,
    };

    {
        let snd = req.data::<super::model::State>().unwrap().stop.lock().await;
        if let Some(handle) = &*snd {
            if let Err(why) = handle.new_frame(from, to).await {
                error!(?why, "failed to send data to ffmpeg");
                return Err(Error::Unknown);
            };
        }
    }

    super::make_ok(&format!(
        "frame {} through {} accepted and queued for encoding",
        from, to
    ))
    .map_err(From::from)
}

pub async fn stop(req: Request<Body>) -> Result<Response<Body>, Error> {
    check_access_cond!(@running req);

    _stop_internal(false, "encoding stopped.", req).await
}

pub async fn cancel(req: Request<Body>) -> Result<Response<Body>, Error> {
    check_access_cond!(@running req);

    _stop_internal(true, "encoding canceled.", req).await
}

async fn _stop_internal(
    is_cancel: bool,
    msg: &'static str,
    req: Request<Body>,
) -> Result<Response<Body>, Error> {
    {
        let snd = req
            .data::<super::model::State>()
            .unwrap()
            .stop
            .lock()
            .await
            .take();

        if let Some(handle) = snd {
            if is_cancel {
                handle.cancel();
            } else if let Err(why) = handle.stop().await {
                error!(?why, "failed to distribute data to ffmpeg");
            };

            if req
                .data::<super::model::State>()
                .unwrap()
                .shutdown
                .lock()
                .await
                .take()
                .unwrap()
                .send(handle)
                .is_err()
            {
                error!("failed to send handle for shutdown");
            };
        }
    }

    super::make_ok(msg).map_err(From::from)
}
