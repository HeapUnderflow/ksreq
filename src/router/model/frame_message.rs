#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub enum FrameMessage {
    Frame { idx: usize, to: usize },
    Stop,
}
