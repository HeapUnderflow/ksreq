use std::{fmt, sync::Arc};

use chrono::{DateTime, Local};
use parking_lot::Mutex;

#[derive(Debug)]
pub struct Status {
    data: Arc<Mutex<StatusData>>,
}

impl Clone for Status {
    fn clone(&self) -> Self {
        Self {
            data: Arc::clone(&self.data),
        }
    }
}

impl Default for Status {
    fn default() -> Self {
        Self {
            data: Arc::new(Mutex::new(StatusData {
                frames_recorded: 0,
                current_file:    None,
                errors:          Vec::with_capacity(8),
                status:          StatusState::Waiting,
            })),
        }
    }
}

impl Status {
    pub fn data(&self) -> &Arc<Mutex<StatusData>> { &self.data }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct StatusData {
    pub frames_recorded: usize,
    pub current_file:    Option<String>,
    pub errors:          Vec<ErrorMessage>,
    pub status:          StatusState,
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct ErrorMessage {
    datetime: DateTime<Local>,
    location: &'static str,
    message:  String,
}

impl ErrorMessage {
    pub fn new_string(loc: &'static str, message: String) -> Self {
        ErrorMessage {
            datetime: Local::now(),
            location: loc,
            message,
        }
    }

    pub fn time(&self) -> String {
        self.datetime.to_rfc2822()
    }

    pub fn location(&self) -> &'static str { 
        self.location
    }

    pub fn message(&self) -> &str {
        &self.message
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub enum StatusState {
    Waiting,
    Recording,
    Stopped,
    Canceled,
}

impl fmt::Display for StatusState {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = match *self {
            StatusState::Waiting => "Waiting",
            StatusState::Recording => "Recording",
            StatusState::Stopped => "Stopped",
            StatusState::Canceled => "Canceled",
        };
        write!(f, "{}", s)
    }
}
