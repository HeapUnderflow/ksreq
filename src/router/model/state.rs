use crate::router::ffmpeg::EncoderHandle;
use tokio::sync::{oneshot, Mutex};

pub struct State {
    pub shutdown: Mutex<Option<oneshot::Sender<EncoderHandle>>>,
    pub stop:     Mutex<Option<EncoderHandle>>,
}

impl State {
    pub fn new(shutdown: oneshot::Sender<EncoderHandle>) -> Self {
        Self {
            shutdown: Mutex::new(Some(shutdown)),
            stop:     Mutex::new(None),
        }
    }
}
