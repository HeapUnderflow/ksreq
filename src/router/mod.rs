mod ffmpeg;
mod model;
mod routes;
mod sfile;
mod simple_routes;

pub use self::ffmpeg::EncoderHandle;
use hyper::{
    header::{self, HeaderName, HeaderValue},
    Body,
    Response,
    StatusCode,
};
use routerify::{Middleware, RequestInfo, Router, RouterService};
use tokio::sync::oneshot;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("{0} is not a valid header value")]
    InvalidHeaderValue(#[from] header::InvalidHeaderValue),
    #[error("hyper error")]
    Hyper(#[from] hyper::http::Error),
    #[error("no state data")]
    NoStateData,
    #[error("unknown error")]
    Unknown,
}

async fn default_header_middleware(mut res: Response<Body>) -> Result<Response<Body>, Error> {
    let hdr = res.headers_mut();
    hdr.insert(
        HeaderName::from_static("server"),
        HeaderValue::from_static(crate::SERVER_VERSION),
    );

    if hdr.contains_key("x-powered-by") {
        hdr.remove("x-powered-by");
    }

    Ok(res)
}

pub fn get_service(shutdown_oneshot: oneshot::Sender<EncoderHandle>) -> RouterService<Body, Error> {
    RouterService::new(routes(shutdown_oneshot)).expect("failed to create router service")
}

pub fn routes(shutdown_oneshot: oneshot::Sender<EncoderHandle>) -> Router<Body, Error> {
    Router::builder()
        .data(model::State::new(shutdown_oneshot))
        .data(model::Status::default())
        .middleware(Middleware::post_with_info(log))
        .middleware(Middleware::post(default_header_middleware))
        .get("/alive", simple_routes::alive)
        .get("/health", routes::health)
        .post("/start", routes::start)
        .get("/frame/:fspec", routes::frame)
        .get("/stop", routes::stop)
        .get("/cancel", routes::cancel)
        .get("/static/:file", simple_routes::static_files)
        .build()
        .expect("failed to create router")
}

async fn log(res: Response<Body>, reqi: RequestInfo) -> Result<Response<Body>, Error> {
    info!(r.path=%reqi.uri().path(), r.args=?reqi.uri().query());
    Ok(res)
}

#[derive(Debug, serde::Serialize)]
pub struct RespStruct<'s> {
    status:  RespType,
    message: &'s str,
}

#[derive(Debug, serde::Serialize)]
pub enum RespType {
    #[serde(rename = "success")]
    Success,
    #[serde(rename = "error")]
    Error,
}

pub fn make_error(e: impl std::fmt::Debug) -> Result<Response<Body>, hyper::http::Error> {
    let message = format!("{:?}", e);

    // assuming unwrap on this as all error data should be encodable as string and
    // utf8 without issue
    let data = serde_json::to_string(&RespStruct {
        status:  RespType::Error,
        message: &message,
    })
    .unwrap();

    Response::builder()
        .status(StatusCode::BAD_REQUEST)
        .header(header::CONTENT_TYPE, HeaderValue::from_static("application/json"))
        .body(Body::from(data))
}

pub fn make_ok(msg: &str) -> Result<Response<Body>, hyper::http::Error> {
    // assuming unwrap on this as all error data should be encodable as string and
    // utf8 without issue
    let data = serde_json::to_string(&RespStruct {
        status:  RespType::Success,
        message: msg,
    })
    .unwrap();

    Response::builder()
        .status(StatusCode::OK)
        .header(header::CONTENT_TYPE, HeaderValue::from_static("application/json"))
        .body(Body::from(data))
}
