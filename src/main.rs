#![feature(result_flattening)]

#[macro_use]
extern crate tracing;

use crate::router::EncoderHandle;
use anyhow::Context;
use hyper::{Client, Server, StatusCode, Uri};
use std::{env, net::SocketAddr};
use tokio::{process::Command, sync::oneshot};
use tracing::Instrument;

const LISTEN_URL: &str = "localhost:12345";
pub const SERVER_VERSION: &str = concat!(env!("CARGO_PKG_NAME"), "/v", env!("CARGO_PKG_VERSION"));

macro_rules! location {
    () => {
        concat!(file!(), "#", line!(), ":", column!())
    };
}

macro_rules! statuserror {
    ($e:expr) => {
        crate::router::model::ErrorMessage::new_string(location!(), format!("{}", $e))
    };
    ($e:expr, $why:expr) => {
        crate::router::model::ErrorMessage::new_string(location!(), format!("{}: {}", $why, $e))
    };
}

mod router;

pub enum Endpoints {
    None,
    Health,
}

pub fn get_uri(edp: Endpoints) -> Uri {
    let uri = Uri::builder().authority(LISTEN_URL).scheme("http");
    match edp {
        Endpoints::None => uri.path_and_query("/").build().unwrap(),
        Endpoints::Health => uri.path_and_query("/alive").build().unwrap(),
    }
}

fn main() {
    tracing_subscriber::fmt::init();
    let res = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .thread_name(concat!(env!("CARGO_PKG_NAME"), "-worker"))
        .build()
        .expect("failed to build runtime")
        .block_on(program())
        .context("program failed");

    if let Err(why) = res {
        error!("program failed: {:?}", why);
        std::process::exit(1);
    }
}

#[instrument(name = "main")]
async fn program() -> anyhow::Result<()> {
    let is_server = env::args().nth(1).map(|arg| arg == "-fg").unwrap_or(false);

    if !is_server {
        if is_server_alive().await {
            info!("server is already alive, ignoring");
            return Ok(());
        }

        info!("server not running, starting...");
        Command::new(env::args().next().unwrap())
            .arg("-fg")
            .spawn()
            .context("spawning child process")?;
    } else {
        server().await?;
    }

    Ok(())
}

#[instrument]
async fn server() -> anyhow::Result<()> {
    info!("starting server...");

    let (sd_tx, sd_rx) = oneshot::channel::<EncoderHandle>();

    let service = router::get_service(sd_tx);

    let addr = SocketAddr::from(([127, 0, 0, 1], 12345));
    let server = Server::bind(&addr).serve(service).with_graceful_shutdown(
        async move {
            let handle = sd_rx.await;
            if let Ok(handle) = handle {
                debug!("recieved shutdown handle");
                if !handle.is_done().await {
                    debug!("handle is not done yet, waiting for handle to complete");
                    if let Err(why) = handle.done().await {
                        error!(?why, "failed to wait for ffmpeg to gracefully shut down");
                    };
                }
            }
        }
        .instrument(error_span!("shutdown_waiter")),
    );

    server.await.context("error while running server")
}

/// Test if the server is already running
#[instrument]
async fn is_server_alive() -> bool {
    debug!("attempting to connect to server");
    let cl = Client::new();
    let res = cl.get(get_uri(Endpoints::Health)).await;
    match res {
        Ok(r) => r.status() == StatusCode::OK,
        Err(why) => {
            debug!(?why, "test failed");
            false
        },
    }
}
