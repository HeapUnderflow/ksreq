{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  nativeBuildInputs = with pkgs; [
    pkgconfig cacert
  ];
  buildInputs = with pkgs; [
    # These deps should suffice for now
    binutils clang gnumake openssl
  ];

  RUST_LOG = "info,ksreq=trace";
  RUST_BACKTRACE = 1;
}
